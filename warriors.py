from pytz import timezone


class Warrior:
    def __init__(self, name, aliases, discord_id, timezone):
        self.name = name
        self.aliases = aliases
        self.discord_id = discord_id
        self.timezone = timezone
        self.mention = "<@"+discord_id+">"


warriors = [Warrior("Havoc", ["havoc", "havocx42"], "86484456327634944", timezone('Europe/Dublin')),
            Warrior("Eamo", ["eamon", "eamo!", "eamo"], "192371100016050185", timezone('Europe/Dublin')),
            Warrior("Chaxtreme", ["chaxtreme", "chad", "chax"], "196947270724222977", timezone('Europe/Dublin')),
            Warrior("Mariodroepie", ["mariodroepie", "mario"], "211228344706924544", timezone('Europe/Amsterdam')),
            Warrior("Okopolitan", ["okopolitan", "oko"], "147026523520499712", timezone('Europe/Amsterdam')),
            Warrior("Wevansly", ["wevansly", "wez", "wev"], "245193268139130880", timezone('Europe/Dublin')),
            Warrior("üfük", ["üfük", "ufu"], "217341069862502400", timezone('Europe/Istanbul'))
            ]


def lookup_by_alias(alias):
    for warrior in warriors:
        if alias.lower().strip() in [a.lower() for a in warrior.aliases]:
            return warrior
    return Warrior(alias, [], alias, timezone('UTC'))
