import argparse

import config
import discordClient
import persistance
import wig

parser = argparse.ArgumentParser(description="Wood/Waffle Tier Warriors related stuff")
parser.add_argument('command', type=str, help='The action to perform')
parser.add_argument('--server', type=str, help='The discord server to use')
parser.add_argument('--channel', type=str, help='The discord channel to use')
parser.add_argument('--url', type=str, help='The results url to save')
parser.add_argument('--code', type=str, help='The WiG code to save')
parser.add_argument('--start', type=str, help='The start epoch of the WiG')
parser.add_argument('--end', type=str, help='The end epoch of the WiG')
args = parser.parse_args()
if args.server:
    config.discord_server = args.server
if args.channel:
    config.discord_channel = args.channel

if args.command == "wig":
    new_wig = wig.create_wig(config)
    persistance.save_wig(new_wig)

    message = "@everyone When is Good for next week: " + new_wig.url + " \nResults: " + new_wig.results_url
    discordClient.send_message_and_close(message)

if args.command == "listen":
    discordClient.listen()

if args.command == "load":
    persistance.save_wig(wig.Wig(args.start, args.end, args.code, args.url))

if args.command == "remind":
    discordClient.remind_wig()

if args.command == "list":
    persistance.list()

