import pickle
import wig
from event import Event
import pprint as pp

events = []
wigs = []


def save_wig(new_wig):
    if new_wig not in wigs:
        wigs.append(new_wig)
    with open("wigs.dat", "wb") as wf:
        pickle.dump(wigs, wf)


def save_event(new_event):
    if new_event not in events:
        events.append(new_event)
    with open("events.dat", "wb") as ef:
        pickle.dump(events, ef)


def reload_events():
    try:
        global events
        with open("events.dat", "rb") as ef:
            events = pickle.load(ef)
    except FileNotFoundError:
        print("no events.dat file found")

def list():
    reload_events()
    for event in events:
        event.print()


reload_events()
try:
    with open("wigs.dat", "rb") as wf:
        wigs = pickle.load(wf)
except FileNotFoundError:
    print("no wigs.dat file found")
