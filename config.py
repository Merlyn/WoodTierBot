import configparser

configparser = configparser.ConfigParser()
configparser.read('config.ini')
discord_token = configparser['DISCORD']['token']
discord_server = configparser['DISCORD']['server']
discord_channel = configparser['DISCORD']['channel']
local_timezone = configparser['GENERAL']['timezone']

wig_user = configparser['WIG']['user']
wig_pass = configparser['WIG']['pass']
wig_event_name = configparser['WIG']['event_name']
