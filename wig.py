import datetime
from collections import defaultdict

import pytz as pytz
import requests
import re

import persistance
import warriors


class Wig:
    def __init__(self, start, end, code, results_url):
        self.results_url = results_url
        self.code = code
        self.url = "http://whenisgood.net/" + code
        self.start = start
        self.end = end


def convert_to_wig_epoch(time):
    return str(int(time.timestamp())) + "000"


def convert_to_datetime(timestamp):
    return pytz.timezone('Europe/Dublin').localize(datetime.datetime.fromtimestamp(int(timestamp[:-3])))


def next_weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0:  # Target day already happened this week
        days_ahead += 7
    return d + datetime.timedelta(days_ahead)


def extract_answer_lines(text):
    lines = text.split("\r\n")
    number_name = {}
    answers = {}
    for line in lines:
        name_m = re.search("(r\d+).name = \"(.*)\";", line)
        candos_m = re.search("(r\d+).myCanDos = \"([\d,]*)\"\.split.*;", line)
        if name_m:
            number_name[name_m.group(1)] = name_m.group(2)
            answers[name_m.group(2)] = ''
        if candos_m:
            answers[number_name[candos_m.group(1)]] = candos_m.group(2)
    return answers


def get_wig_for_time(time):
    timestamp = convert_to_wig_epoch(time)
    t_int = int(timestamp)
    for e in persistance.wigs:
        if int(e.start) < t_int < int(e.end):
            return e
    return None


def find_available(time):
    time = time.replace(microsecond=0, second=0, minute=0)
    timestamp = convert_to_wig_epoch(time)
    wig = get_wig_for_time(time)
    session = requests.Session()
    resultsr = session.get(wig.results_url)
    answers = extract_answer_lines(resultsr.text)
    options = []
    for name, answer in answers.items():
        if timestamp in answer:
            options.append(name)
    return options


def get_windows():
    now = datetime.datetime.now()
    now_epoch = convert_to_wig_epoch(now)
    availability = defaultdict(list)
    for w in persistance.wigs:
        if int(w.end) > int(now_epoch):
            session = requests.Session()
            resultsr = session.get(w.results_url)
            answers = extract_answer_lines(resultsr.text)
            for name, times in answers.items():
                for a_time in times.split(','):
                    availability[a_time].append(warriors.lookup_by_alias(name))

    possible_times = [convert_to_datetime(time) for (time, wars) in availability.items() if len(wars) >= 5]
    # convert to days and hours
    possible_hours_days = defaultdict(list)
    tz = pytz.timezone('Europe/Amsterdam')
    dayfmt = "%A"
    hourfmt = "%H"

    for a_time in possible_times:
        possible_hours_days[a_time.astimezone(tz).strftime(dayfmt)].append(int(a_time.astimezone(tz).strftime(hourfmt)))
    response = "Available times:\n"
    day_order = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    for day in day_order:
        unsorted_hours = possible_hours_days[day]
        if len(unsorted_hours) > 1:
            hours = sorted(unsorted_hours)
            response += day + ": "
            window_start = hours[0]
            previous_hour = hours[0]
            windows = []
            for hour in hours[1:]:
                if hour != previous_hour + 1:
                    windows.append((window_start, previous_hour))
                    window_start = hour
                previous_hour = hour
            windows.append((window_start, hours[-1]))
            response += ", ".join([str(start) + ":00 - " + str(end) + ":00" for (start, end) in windows]) + "\n"
        elif len(unsorted_hours) == 1:
            response += day + ": " + str(unsorted_hours[0]) + ":00\n"
    return response


def list_answerees(results_url):
    session = requests.Session()
    resultsr = session.get(results_url)
    answers = extract_answer_lines(resultsr.text)
    return answers.keys()


def create_wig(config):
    wig_user = config.wig_user
    wig_pass = config.wig_pass
    wig_event_name = config.wig_event_name
    today = datetime.date.today()
    monday = next_weekday(today, 0)
    week = [monday + datetime.timedelta(i) for i in range(0, 7)]
    hours = [datetime.time(i, tzinfo=pytz.utc) for i in range(12, 24)]
    times = [str(int(datetime.datetime.combine(day, hour).timestamp())) + "000" for day in week for hour in hours]

    session = requests.Session()

    print("Logging in...")
    loginr = session.post("http://whenisgood.net/Login",
                          data={'loginEmail': wig_user, 'loginPassword': wig_pass, 'intercepted': ''})
    print(loginr.status_code)
    print("Creating new event from " + str(monday) + " to " + str(week[6]))
    newr = session.get("http://whenisgood.net/Create")
    id = 'input name="url" size="20" maxLength="50" value="'
    loc = newr.text.find('input name="url" size="20" maxLength="50" value="') + len(id)
    end = newr.text.find("\"", loc)
    code = newr.text[loc:end]

    eventdata = dict(SetLocale='en_US', gridSize='14', useTimezones='true', timezoneId='Europe/Dublin',
                     eventName=wig_event_name,
                     startDate='Jun 18, 2018', endDate='Jun 24, 2018', showDay0='on', showDay1='on', showDay2='on',
                     showDay3='on', showDay4='on', showDay5='on', showDay6='on', startTime='12', endTime='24',
                     startOffice='19', endOffice='23', url=code,
                     topText='Below are the proposed START TIMES of the session. Paint over all that are good for you. ',
                     duration='', durationFactor='1', sendAlerts='on', canDos=",".join(times),
                     resolution='60', timezoneWas='GMT', previousLocale='en_US', optionsPresent='true',
                     previousStartDate='Jun 10, 2018', previousEndDate='Jun 30, 2018', previousResolution='60',
                     go='true',
                     sync='')
    creater = session.post("http://whenisgood.net/Create", data=eventdata)
    toggler = session.post("http://whenisgood.net/ToggleShareResults", data=dict(event=code, shareResults='on'))
    print("Creating event...")
    print(creater.url)
    results_url = creater.url.replace("links", "results")
    print("Created event with code:" + code)
    return Wig(times[0], times[-1], code, results_url)
