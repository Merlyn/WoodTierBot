import datetime
import pytz
class Event:
    def __init__(self, datetime, warriors, message, name):
        self.datetime = datetime
        self.warriors = warriors
        self.message = message
        self.name = name
        self.reminder_sent = False

    def print(self):
        print("datetime:"+str(self.datetime)+", warriors:"+str(self.warriors)+", message:"+str(self.message)+", name:"+str(self.name))
        print(datetime.datetime.combine(datetime.date.today(), self.datetime.timetz()) - pytz.timezone("UTC").localize(datetime.datetime.now()))
